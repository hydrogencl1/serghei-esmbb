# load modules at JSC
module load Stages/2023  NVHPC  ParaStationMPI NCO

# convert nc file with variables stored as doubles into floats
ncap2 -s 'z=float(z); h=float(h); u=float(u); v=float(v)' output.nc float.nc

# deflate nc file
ncks -7 -L 1 float.nc float_deflated.nc
