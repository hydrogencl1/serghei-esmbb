#include "serghei.h"
class damBreakConfig{
	public:
	geometry::point p; //point defining the dam break
	real size; //distance defining a dam break
	real hLow, hHigh; // dam break depths
};

void createDamBreakX(Domain &dom, damBreakConfig &db){
	// domain definition
	dom.nx_glob = 100;
	dom.ny_glob = 4;
	dom.dxConst = 0.5;
	dom.BCtype = BC_REFLECTIVE;
	// minimal parameters
	dom.simLength= 8;
	// initial conditions
	db.p(_X)= dom.nx_glob * dom.dxConst * 0.5;	
	db.hHigh = 4.;	
	db.hLow = 1.;
}

void damBreakX(const Domain &dom, State &state, const damBreakConfig &db){
	Kokkos::parallel_for("damBreakCreator",dom.nCell,KOKKOS_LAMBDA (int iGlob){
 	 	int ii = dom.getIndex(iGlob);
		state.h(ii) = db.hLow;
		if(dom.getCellCenter(iGlob)(_X) <= db.p(_X)) state.h(ii) = db.hHigh;
	});
}

void createDamBreakCircle(Domain &dom, damBreakConfig &db){
	// domain definition
	dom.nx_glob = 100;
	dom.ny_glob = 100;
	dom.dxConst = 0.5;
	dom.BCtype = BC_REFLECTIVE;
	// minimal parameters
	dom.simLength= 8;
	// initial conditions
	db.p(_X)= dom.nx_glob * dom.dxConst * 0.5;	
	db.p(_Y)= dom.ny_glob * dom.dxConst * 0.5;	
	db.hHigh = 4.;	
	db.hLow = 1.;
	db.size = 10;
}
void damBreakCircle(const Domain &dom, State &state, const damBreakConfig &db){
	Kokkos::parallel_for("damBreakCreator",dom.nCell,KOKKOS_LAMBDA (int iGlob){
 	 	int ii = dom.getIndex(iGlob);
		state.h(ii) = db.hLow;
    real d = geometry::distance(dom.getCellCenter(iGlob),db.p);
		if(d <= db.size) state.h(ii) = db.hHigh;
	});
}

int main(int argc, char** argv) {

	// Read command line arguments 
	 if (argc != 4){
	  std::cerr << RERROR "The program is run as: ./nprogram inputFolder/ outputFolder/ Nthreads" << std::endl;
		  return 1;
	}

	{
	int found=0;
	SERGHEI serghei;

	std::string testCase = argv[1];
  std::cout << BOLD "INTEGRATION TEST - " << testCase << RESET << std::endl;

	serghei.outFolder = "./" + testCase + "/";

	serghei.par.nthreads = atoi(argv[2]);

	serghei.init.read = 0;	// override reading of files

	damBreakConfig db;

	// domain decomposition
	serghei.par.nproc_x = 1;
	serghei.par.nproc_y = 1;
	serghei.par.nranks = 1;
	// minimal parameters
	serghei.dom.cfl = 0.5;
	serghei.io.outFreq =  1;
	serghei.io.outFormat = OUT_NETCDF;

	if(!testCase.compare("damBreakX")){
		createDamBreakX(serghei.dom, db);
		found++;
	}
	if(!testCase.compare("damBreakCircle")){
		createDamBreakCircle(serghei.dom,db);
		found++;
	}

	if(!found){
		std::cerr << RERROR << "Invalid test case " << std::endl;
		return 1;
	}

	std::cout << GOK << "Domain size: " << serghei.dom.nx_glob << "x" << serghei.dom.ny_glob << std::endl;
	std::cout << GOK << "Number of MPI ranks: " << serghei.par.nranks <<std::endl;
	
	if(!serghei.start(argc, argv)) return 1;
	
	std::cout << GOK << "Number of cells: "<< serghei.dom.nCell << std::endl;
	std::cout << BDASH << "Creating initial state" << std::endl;

	if(!testCase.compare("damBreakX")) damBreakX(serghei.dom, serghei.state, db);
	if(!testCase.compare("damBreakCircle")) damBreakCircle(serghei.dom, serghei.state, db);

	if(!serghei.compute()) return 1;
	if(!serghei.finalise()) return 1;
  
	} // scope guard required to ensure serghei destructor is called

	Kokkos::finalize();
  MPI_Finalize();

	return 0;
}